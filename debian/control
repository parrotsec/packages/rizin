Source: rizin
Section: misc
Priority: optional
Maintainer: Parrot Team <team@parrotsec.org>
Uploaders: Nong Hoang Tu <dmknght@parrotsec.org>
Build-Depends: debhelper-compat (= 13),
               libcapstone-dev,
               libgmp-dev,
               libkvm-dev [kfreebsd-any],
               libmagic-dev,
               libuv1-dev,
               libxxhash-dev,
               libzip-dev,
               liblz4-dev,
               meson,
               pkg-config,
               python3,
               python3-setuptools,
               zlib1g-dev,
               autotools-dev,
               python3-yaml
Standards-Version: 4.6.2
Homepage: https://rizin.re/
Vcs-Browser: https://gitlab.com/parrotsec/packages/rizin/
Vcs-Git: https://gitlab.com/parrotsec/packages/rizin/.git
Rules-Requires-Root: no

Package: rizin
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: A fork of the radare2 reverse engineering framework
 Rizin is portable and it can be used to analyze binaries,
 disassemble code, debug programs, as a forensics tool,
 as a scriptable command-line hexadecimal editor able to
 open disk files, and much more!


Package: librizin-core
Section: libs
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}, librizin-common (>= ${source:Version})
Multi-Arch: same
Description: libraries from the rizin suite
 The project aims to create a complete, portable, multi-architecture,
 unix-like toolchain for reverse engineering.
 .
 It is composed by an hexadecimal editor (rizin) with a wrapped IO
 layer supporting multiple backends for local/remote files, debugger
 (OS X, BSD, Linux, W32), stream analyzer, assembler/disassembler (rasm)
 for x86, ARM, PPC, m68k, Java, MSIL, SPARC, code analysis modules and
 scripting facilities. A bindiffer named radiff, base converter (rax),
 shellcode development helper (rasc), a binary information extractor
 supporting PE, mach0, ELF, class, etc. named rabin, and a block-based
 hash utility called rahash.
 .
 This package provides the libraries from rizin.

Package: librizin-dev
Section: libdevel
Architecture: any
Depends: ${misc:Depends}, librizin-core (= ${binary:Version}),
         libcapstone-dev, libmagic-dev, libuv1-dev, liblz4-dev,
         libzip-dev,
Description: Devel files from the rizin suite
 The project aims to create a complete, portable, multi-architecture,
 unix-like toolchain for reverse engineering.
 .
 It is composed by an hexadecimal editor (rizin) with a wrapped IO
 layer supporting multiple backends for local/remote files, debugger
 (OS X, BSD, Linux, W32), stream analyzer, assembler/disassembler (rasm)
 for x86, ARM, PPC, m68k, Java, MSIL, SPARC, code analysis modules and
 scripting facilities. A bindiffer named radiff, base converter (rax),
 shellcode development helper (rasc), a binary information extractor
 supporting PE, mach0, ELF, class, etc. named rabin, and a block-based
 hash utility called rahash.
 .
 This package provides the devel files from rizin.

Package: librizin-common
Multi-Arch: foreign
Architecture: all
Depends: ${misc:Depends}
Description: arch independent files from the rizin suite
 The project aims to create a complete, portable, multi-architecture,
 unix-like toolchain for reverse engineering.
 .
 It is composed by an hexadecimal editor (rizin) with a wrapped IO
 layer supporting multiple backends for local/remote files, debugger
 (OS X, BSD, Linux, W32), stream analyzer, assembler/disassembler (rasm)
 for x86, ARM, PPC, m68k, Java, MSIL, SPARC, code analysis modules and
 scripting facilities. A bindiffer named radiff, base converter (rax),
 shellcode development helper (rasc), a binary information extractor
 supporting PE, mach0, ELF, class, etc. named rabin, and a block-based
 hash utility called rahash.
 .
 This package provides the arch independent files from rizin.
